<section class="counter" style="background: url(assets/images/banner2.jpg) center; background-size: cover; background-attachment: fixed;">
      <div class="overlay"></div>
      <div class="container content">
        <div class="row">
          <div class="col-sm-3">
            <div class="icon"><i class="pe-7s-clock pe-4x"></i></div>
            <h3 class="timer" data-to="1500" data-speed="3000" data-from="0">1500</h3>        
            <h4>Hours Of Work</h4>
          </div>
          <div class="col-sm-3">
            <div class="icon"><i class="pe-7s-coffee pe-4x"></i></div>
            <h3 class="timer" data-to="45" data-speed="3000" data-from="0">45</h3>
            <h4>Cups of Coffee</h4>
          </div>
          <div class="col-sm-3">
            <div class="icon"><i class="pe-7s-rocket pe-4x"></i></div>
            <h3 class="timer" data-to="178" data-speed="3000" data-from="0">178</h3>
            <h4>Project Done</h4>
          </div>
          <div class="col-sm-3">
            <div class="icon"><i class="pe-7s-add-user"></i></div>
            <h3 class="timer" data-to="32" data-speed="3000" data-from="0">32</h3>
            <h4>Clients Worked</h4>
          </div>
        </div>      
      </div>
    </section><!--/.timer -->