    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ route('pages.home') }}"></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ route('pages.home') }}">Home</a></li>
            <li><a href="{{ route('pages.aboutme') }}">About Me</a></li>
            <li><a href="{{ route('project.list')}}">Project</a></li>
            <li><a href="{{ route('exps.list') }}">Experience</a></li>
            <li><a href="{{ route('blog.list')}}">Blog</a></li>
            <li><a href="{{ route('pages.contact')}}">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>