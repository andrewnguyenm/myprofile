<section class="testimonial text-center" id="testimonial" style="background: url(assets/images/banner3.jpg) center; background-size: cover; background-attachment: fixed;">
      <div class="overlay"></div>
      <div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
        <div class="container">
          <div class="carousel-inner">
            <div class="item active">
              <div class="photo">
                <img src="assets/images/person12.jpg" alt="Testimonial">
              </div>
              <div class="text">
                " Cuộc sống vốn không công bằng - Hãy tập quen dần với điều đó. "
              </div>
              <div class="separator-container">
                <div class="separator"><div class="shape"></div></div>
              </div>
              <div class="name">
                <span class="textbold">Bill Gates</span>, Inventor of Microsoft
              </div>
            </div><!--/.item -->

            <div class="item">
              <div class="photo">
                <img src="assets/images/person22.jpg" alt="Testimonial">
              </div>
              <div class="text">
                " Stay hungry, stay foolish. "
              </div>
              <div class="separator-container">
                <div class="separator"><div class="shape"></div></div>
              </div>
              <div class="name">
                <span class="textbold">Steve Jobs</span>, CEO of Apple
              </div>
            </div><!--/.item -->
          </div><!--/.carousel-inner -->
        </div><!--/.container -->

        <!-- Controls -->
        <a class="left carousel-control" href="#testimonial-carousel" role="button" data-slide="prev" data-wow-delay="1s">
          <div class="control-circle left"><i class="fa fa-angle-left"></i></div>
        </a>
        <a class="right carousel-control" href="#testimonial-carousel" role="button" data-slide="next" data-wow-delay="1s">
          <div class="control-circle right"><i class="fa fa-angle-right"></i></div>
        </a>
      </div><!--/.testimonial-carousel -->
    </section><!--/.testimonial -->