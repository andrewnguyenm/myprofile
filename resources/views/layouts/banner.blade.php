<section class="banner" id="home">
      <div class="overlay"></div>
      <div class="banner-content">
        <div class="container">
          <div class="col-md-12">
            <div class="subtitle">
              <p>I Am Anthony Nguyen</p>
            </div>
            <div class="title">
              <h1 id="typed"></h1>
            </div>
            <div class="buttons">
              <a href="{{ route('pages.aboutme') }}" class="def-btn">More About Me</a>
            </div>
          </div>
        </div><!-- /.container -->

        <div class="scroll-info">
          <a href="#about">
            <div class="shape">
              <i class="fa fa-angle-down"></i>
            </div>
          </a>
        </div><!--/.scroll-info -->
      </div><!-- /.banner-content -->
    </section><!-- /.banner -->