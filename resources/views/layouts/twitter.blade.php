<section class="twitter-feed" style="background: url(assets/images/banner4.jpg) center; background-size: cover; background-attachment: fixed;">
      <div class="overlay"></div>
      <div class="logo-container"><div class="logo"><i class="fa fa-facebook"></i></div></div>
      <div id="twitter-carousel" class="carousel slide" data-ride="carousel">
        <div class="container">
          <div class="carousel-inner">
            <div class="item active">
              <div class="tweet">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div><!--/.item -->
            <div class="item">
              <div class="tweet">
                <p>Lorem ipsum dolor sit amet, <span class="textbold">@owlcity</span> adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div><!--/.item -->
          </div><!--/.carousel-inner -->
          <div class="time">3 Days Ago From <a href="#">@suavedigital_ID</a></div>
        </div><!--/.container -->
      </div><!--/.twitter-carousel -->
    </section><!--/.twitter-feed -->