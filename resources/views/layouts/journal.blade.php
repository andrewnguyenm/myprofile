<section class="journal bg-section" id="news">
      <div class="container">
        <div class="title section-title text-center wow animated fadeIn">
          <div class="title-icon-container">
            <div class="title-icon"><i class="pe-7s-news-paper pe-2x pe-va"></i></div>
          </div>
          <h1>My Blog</h1>
          <h2>Stay up to date</h2>
        </div>

        <div class="description">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
        </div>
          <article class="col-md-4 col-sm-6 masonry wow animated fadeIn">
            <div class="journal-content">
              <div class="media">
                <img src="assets/images/news1.jpg" alt="news content">
              </div>
              <div class="detail">
                <div class="category-wrap"><div class="category"><i class="pe-7s-photo"></i></div></div>
                <div class="title"><a href="journal-single.html"><h3>Lorem Ipsum dolor sit amet</h3></a></div>
                <div class="info">
                  <i class="pe-7s-user"></i><a href="#">Administrator</a> <i class="pe-7s-ticket"></i> <a href="#">Freebies</a> <i class="pe-7s-comment"></i> <a href="#">3 Comments</a>
                </div>
                <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                <div class="post-date">12 Aug</div>
              </div>
            </div>
          </article>
          
        </div><!--/#masonry-container -->
      </div><!--/.container -->

      <div class="container">
        <div class="col-md-12">
          <div class="show-more text-center">
            <a href="{{ route('blog.list') }}" class="def-btn">Show More</a>
          </div>
        </div>
      </div>

    </section><!--/.journal -->