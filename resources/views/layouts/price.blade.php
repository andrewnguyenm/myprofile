<section class="pricing" id="pricing">
      <div class="container">
        <div class="title section-title text-center wow animated fadeIn">
          <div class="title-icon-container">
            <div class="title-icon"><i class="pe-7s-news-paper pe-2x pe-va"></i></div>
          </div>
          <h1>Pricing Table</h1>
          <h2>My Work Rate</h2>
        </div>

        <div class="description">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="pricing-table wow animated fadeInUp" data-wow-delay="0.2s">
            <div class="header"></div>
            <div class="price">
              <div class="pricetext"><h3>Starter</h3><span class="textbold">$5</span> / hours</div>
            </div>
            <div class="list">
              <ul>
                <li><span class="textbold">Up to</span> 5 Users</li>
                <li><span class="textbold">Max</span> 100 items</li>
                <li><span class="textbold">Unlimited</span> QueriesFull</li>
                <li><span class="textbold">Statistics</span></li>
                <li><span class="textbold">Great</span> Support</li>
              </ul>
              <a href="#" class="def-btn">Get This Plan</a>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="pricing-table wow animated fadeInUp" data-wow-delay="0.4s">
            <div class="price">
              <div class="pricetext"><h3>Medium</h3><span class="textbold">$5</span> / hours</div>
            </div>
            <div class="list">
              <ul>
                <li><span class="textbold">Up to</span> 5 Users</li>
                <li><span class="textbold">Max</span> 100 items</li>
                <li><span class="textbold">Unlimited</span> QueriesFull</li>
                <li><span class="textbold">Statistics</span></li>
                <li><span class="textbold">Great</span> Support</li>
              </ul>
              <a href="#" class="def-btn">Get This Plan</a>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="pricing-table active wow animated fadeInUp" data-wow-delay="0.6s">
            <div class="price">
              <div class="pricetext"><h3>Business</h3><span class="textbold">$5</span> / hours</div>
            </div>
            <div class="list">
              <ul>
                <li><span class="textbold">Up to</span> 5 Users</li>
                <li><span class="textbold">Max</span> 100 items</li>
                <li><span class="textbold">Unlimited</span> QueriesFull</li>
                <li><span class="textbold">Statistics</span></li>
                <li><span class="textbold">Great</span> Support</li>
                <li><span class="textbold">Agile</span> Work</li>
              </ul>
              <a href="#" class="def-btn">Get This Plan</a>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="pricing-table wow animated fadeInUp" data-wow-delay="0.8s">
            <div class="price">
              <div class="pricetext"><h3>Pro</h3><span class="textbold">$5</span> / hours</div>
            </div>
            <div class="list">
              <ul>
                <li><span class="textbold">Up to</span> 5 Users</li>
                <li><span class="textbold">Max</span> 100 items</li>
                <li><span class="textbold">Unlimited</span> QueriesFull</li>
                <li><span class="textbold">Statistics</span></li>
                <li><span class="textbold">Great</span> Support</li>
              </ul>
              <a href="#" class="def-btn">Get This Plan</a>
            </div>
          </div>
        </div>

      </div>
    </section>