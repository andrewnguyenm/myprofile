<section class="employment">
      <div class="title section-title bg-section text-center bottom-shape wow animated fadeIn">
        <div class="title-icon-container">
          <div class="title-icon"><i class="pe-7s-portfolio pe-2x pe-va"></i></div>
        </div>
        <h1>My Experience</h1>
        <h2>I Do</h2>
      </div>
        
      <div class="container">
        <div class="timeline-container">
          
          <ul class="timeline">
            <li>
              <time class="timeline-year wow animated fadeIn" data-wow-delay="0.2s"><span>2015</span></time>
              <div class="timeline-icon wow animated fadeIn" data-wow-delay="0.2s">
                <i class="pe-7s-portfolio pe-va"></i>
              </div>
              <div class="timeline-label wow animated fadeIn" data-wow-delay="0.2s">
                <h3>Front End Developer</h3>
                <span class="place textbold">Themeforest Studio</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </li>
            <li>
              <time class="timeline-year wow animated fadeIn" data-wow-delay="0.2s"><span>2014</span></time>
              <div class="timeline-icon wow animated fadeIn" data-wow-delay="0.2s">
                <i class="pe-7s-portfolio pe-va"></i>
              </div>
              <div class="timeline-label wow animated fadeIn" data-wow-delay="0.2s">
                <h3>UI / UX Designer</h3>
                <span class="place textbold">Envato Studio</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliqu
              </div>
            </li>
            <li>
              <time class="timeline-year wow animated fadeIn" data-wow-delay="0.2s"><span>2013</span></time>
              <div class="timeline-icon wow animated fadeIn" data-wow-delay="0.2s">
                <i class="pe-7s-portfolio pe-va"></i>
              </div>
              <div class="timeline-label wow animated fadeIn" data-wow-delay="0.2s">
                <h3>Graphic Design</h3>
                <span class="place textbold">Graphicriver Labs</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliqu
              </div>
            </li>
            <li>
              <time class="timeline-year wow animated fadeIn" data-wow-delay="0.2s"><span>2012</span></time>
              <div class="timeline-icon wow animated fadeIn" data-wow-delay="0.2s">
                <i class="pe-7s-portfolio pe-va"></i>
              </div>
              <div class="timeline-label wow animated fadeIn" data-wow-delay="0.2s">
                <h3>Web Developer</h3>
                <span class="place textbold">Codecanyon Studio</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliqu
              </div>
            </li>
          </ul><!--/.timeline -->
        </div><!--/.timeline-container -->
      </div><!--/.container -->
    </section>