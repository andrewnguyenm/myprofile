<section class="portfolio" id="portfolio">
      <div class="container">
        <div class="title section-title text-center wow animated fadeIn">
          <div class="title-icon-container">
            <div class="title-icon"><i class="pe-7s-rocket pe-2x pe-va"></i></div>
          </div>
          <h1>My Project</h1>
          <h2>Product Well Done</h2>
        </div>
        <div class="description">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
        </div>
      </div>

      <div class="container">
        <div class="portfoliocontent">

          <div class="item col-md-4 col-sm-6 col-xs-12 webdesign">
            <a href="{{ route('project.detail') }}">
              <div class="content">
                <div class="overlay">
                  <div class="valign-container">
                    <div class="text valign-content">
                      <span class="project-name">Beauty Typography</span>
                      <div class="separator-container">
                        <div class="separator"><div class="shape"></div></div>
                      </div>
                      <span class="project-date">1 Jan 2015</span>
                    </div>
                  </div>
                </div>
                <div class="image">
                  <img src="{{ url('assets/images/work1.jpg') }}" alt="Portfolio image">
                </div>
              </div><!--/.content -->
            </a>
          </div><!--/.item -->
        </div><!--/.portfoliocontent -->
      </div><!--/.container -->
      
      <div class="container">
        <div class="col-md-12">
          <div class="show-more text-center">
            <a href="{{ route('project.list') }}" class="def-btn">Show More</a>
          </div>
        </div>
      </div>

    </section><!--/.portfolio -->