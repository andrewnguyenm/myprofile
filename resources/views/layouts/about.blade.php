<section class="about" id="about">
      <div class="title section-title text-center">
        <div class="title-icon-container">
          <div class="title-icon"><i class="pe-7s-id pe-2x"></i></div>
        </div>
        <h1>Anthony Nguyen</h1>
        <h2>Web Developer</h2>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="center">
              <div class="img-relative photo">
                <img alt="My Photo" src="assets/images/me.jpg">
              </div>
              <div class="img-relative photo">
                <img alt="My Photo" src="assets/images/me2.jpg">
              </div>
              <div class="img-relative photo">
                <img alt="My Photo" src="assets/images/me3.jpg">
              </div>
              <div class="img-relative photo">
                <img alt="My Photo" src="assets/images/me4.jpg">
              </div>
            </div>

            <div class="description">
              <div class="text">
                <p>I Am Web Developer, consectetur adipiscing elit. Nullam nec efficitur ipsum. In id feugiat nibh. Proin viverra metus vel orci eleifend eleifend. Duis eu magna ac nisi accumsan viverra ac eu massa. Aenean fermentum at mi vel suscipit. Aenean hendrerit lorem vel magna lobortis volutpat. Proin auctor convallis quam sit amet ullamcorper.</p>
              </div>
            </div>

            <div class="more">
              <ul>
                <li><a href="{{ url('/contact') }}"><i class="pe-7s-phone pe-2x pe-va"></i>Contact Me</a></li>
              </ul>
            </div>
          </div>

        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.features -->