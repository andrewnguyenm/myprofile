<section class="education" id="education">
      <div class="title section-title bg-section text-center bottom-shape wow animated fadeIn">
        <div class="title-icon-container">
          <div class="title-icon"><i class="pe-7s-study pe-2x pe-va"></i></div>
        </div>
        <h1>My Education</h1>
        <h2>I learn</h2>
      </div>
        
      <div class="container">
        <div class="timeline-container">
          
          <ul class="timeline">
            <li>
              <time class="timeline-year wow animated fadeIn" data-wow-delay="0.2s"><span>2012</span></time>
              <div class="timeline-icon wow animated fadeIn" data-wow-delay="0.2s">
                <i class="pe-7s-study pe-va"></i>
              </div>
              <div class="timeline-label wow animated fadeIn" data-wow-delay="0.2s">
                <h3>Graphic Design</h3>
                <span class="place textbold">Harvard University</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </li>
            <li>
              <time class="timeline-year wow animated fadeIn" data-wow-delay="0.2s"><span>2009</span></time>
              <div class="timeline-icon wow animated fadeIn" data-wow-delay="0.2s">
                <i class="pe-7s-study pe-va"></i>
              </div>
              <div class="timeline-label wow animated fadeIn" data-wow-delay="0.2s">
                <h3>Information &amp; Technology</h3>
                <span class="place textbold">Bandung High School</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </li>
            <li>
              <time class="timeline-year wow animated fadeIn" data-wow-delay="0.2s"><span>2006</span></time>
              <div class="timeline-icon wow animated fadeIn" data-wow-delay="0.2s">
                <i class="pe-7s-study pe-va"></i>
              </div>
              <div class="timeline-label wow animated fadeIn" data-wow-delay="0.2s">
                <h3>Mathematics</h3>
                <span class="place textbold">Kebon Kopi Junior High School</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </li>
            <li>
              <time class="timeline-year wow animated fadeIn" data-wow-delay="0.2s"><span>2000</span></time>
              <div class="timeline-icon wow animated fadeIn" data-wow-delay="0.2s">
                <i class="pe-7s-study pe-va"></i>
              </div>
              <div class="timeline-label wow animated fadeIn" data-wow-delay="0.2s">
                <h3>Regular Student</h3>
                <span class="place textbold">Rancabentang Primary School</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </li>
          </ul><!--/.timeline -->
        </div><!--/.timeline-container -->
      </div><!--/.container -->
    </section><!--/.education -->