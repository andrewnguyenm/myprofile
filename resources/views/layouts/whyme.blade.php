<section class="why-me" id="why-me">
      <div class="container">
        
        <div class="col-md-6 text">
          <div class="title"><h1>Why hire me ?</h1></div>
          <div class="tagline">
            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
          </div>
          <div class="description">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi labore et dolore magna aliqua consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua consectetur adipisicing elit, sed do eiusmod.</p>
          </div>
        </div><!--/.text -->

        <div class="col-md-6 skills">
          <div class="title">
            <h3>I am good at</h3>
          </div>
          <div class="skillbars">
            <div class="bar-container">
              <p><i class="icon-pencil pi-icon-left"></i>HTML CSS3</p>
              <div class="meter wow animated fadeInLeft" style="width: 80%"></div>
            </div>
            <div class="bar-container">
              <p><i class="icon-pencil pi-icon-left"></i>Java Script</p>
              <div class="meter wow animated fadeInLeft" style="width: 50%"></div>
            </div>
            <div class="bar-container">
              <p><i class="icon-pencil pi-icon-left"></i>Bootstrap</p>
              <div class="meter wow animated fadeInLeft" style="width: 60%"></div>
            </div>
            <div class="bar-container">
              <p><i class="icon-pencil pi-icon-left"></i>PHP</p>
              <div class="meter wow animated fadeInLeft" style="width: 80%"></div>
            </div>
            <div class="bar-container">
              <p><i class="icon-pencil pi-icon-left"></i>Git</p>
              <div class="meter wow animated fadeInLeft" style="width: 60%"></div>
            </div>
            <div class="bar-container">
              <p><i class="icon-pencil pi-icon-left"></i>Photoshop</p>
              <div class="meter wow animated fadeInLeft" style="width: 50%"></div>
            </div>
            </div>
          </div>
        </div><!--/.skills -->
      </div><!--/.container -->
    </section><!--/.why-me -->