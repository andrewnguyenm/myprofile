   <section class="footer">
      <div class="container">
        <div class="top-text">Follow Me</div>
        <div class="col-md-12 col-sm-12 links">
          <ul>
            <li class="social-icons">
              <a href="https://www.facebook.com/hack3rlov3" data-toggle="tooltip" title="Some tooltip text!">
                <div class="circle">
                  <i class="fa fa-facebook"></i>
                </div>
              </a>
              <a href="https://twitter.com/anthonynguyenm">
                <div class="circle">
                  <i class="fa fa-twitter"></i>
                </div>
              </a>
              <a href="#">
                <div class="circle">
                  <i class="fa fa-google-plus"></i>
                </div>
              </a>
              <a href="#">
                <div class="circle">
                  <i class="fa fa-linkedin"></i>
                </div>
              </a>
              <a href="#">
                <div class="circle">
                  <i class="fa fa-pinterest"></i>
                </div>
              </a>
            </li>
          </ul>
        </div>

        <div class="col-md-12 col-sm-12 copyright">
          <h6>&copy; MePro 2015 All right reserved. Designed by Suave Digital</h6>
        </div>
      </div><!--/.container -->
    </section>