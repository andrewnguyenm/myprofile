<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from demo.suavedigital.com/MePro/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 23 Jul 2015 15:38:39 GMT -->
<head>
    @include('includes.css')
  </head>

  <body data-spy="scroll" data-target=".navbar-collapse" data-offset="80" class="slideshowbody">
    <div class="preloader">
      <div class="image-container">
        <div class="image"><img src="assets/images/preloader.gif" alt=""></div>
      </div>      
    </div>
    <!-- navbar-top -->
    @yield('navbar')
    <!-- Banner Section Begin -->
    @include('layouts.banner')
    <!-- Banner Section End -->
    <!-- Content Section Begin  -->
    @yield('content')
    <!-- Content Section End -->
    <!-- Footer Section Begin  -->
    @include('layouts.footer')
    <!-- Footer Section End  -->

    <!-- JavaScripts -->
        @include('includes.script')
    <!-- Style Switcher - Demo Only Begin -->
<!--     <div class="switcher-box">
      <div class="hiddenbox"><i class="fa fa-gear"></i></div>
      <div class="colorbox-container" style="display: none">
        <div class="head"><i class="pe-7s-paint"></i></div>
        <div class="content">
          <a title="blue" href="javascript:void(0)">
            <div class="colorbox" id="blue"></div>
          </a>
          <a title="green" href="javascript:void(0)">
            <div class="colorbox" id="green"></div>
          </a>
          <a title="orange" href="javascript:void(0)">
            <div class="colorbox" id="orange"></div>
          </a>
          <a title="yellow" href="javascript:void(0)">
            <div class="colorbox" id="yellow"></div>
          </a>
        </div>
      </div>
    </div> -->



  </body>

<!-- Mirrored from demo.suavedigital.com/MePro/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 23 Jul 2015 15:40:22 GMT -->
</html>
