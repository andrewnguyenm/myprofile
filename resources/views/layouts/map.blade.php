<section class="map">
      <div class="map-title">
        <div class="container">
          <h1><span class="textbold">Visit me</span> for some coffee</h1>
        </div>
      </div>
      <div class="map-canvas">
      	<iframe src="https://www.google.com/maps/embed?pb=!1m17!1m11!1m3!1d6331.63829810084!2d105.84215191403433!3d20.98393509527311!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac428b94024f%3A0x2b640967161ac470!2zSOG7hyBUaOG7kW5nIMSQw6BvIFThuqFvIEzhuq1wIFRyw6xuaCBWacOqbiBRdeG7kWMgVOG6vyBBcHJvdHJhaW4!5e1!3m2!1svi!2s!4v1442926218498" width="1350" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </section>