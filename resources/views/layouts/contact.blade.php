<section class="contact" id="contact">
      <div class="container"> 
        <div class="title section-title text-center wow animated fadeIn">
          <div class="title-icon-container">
            <div class="title-icon"><i class="pe-7s-phone pe-2x pe-va"></i></div>
          </div>
          <h1>Get in touch</h1>
          <h2>Whith me</h2>
        </div>

        <div class="description">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.<br/>
          </p>
        </div>    

        <div class="col-md-8 col-md-offset-2">
          <div class="footer-form">
            <form role="form">
              <div class="col-md-12 input-container wow animated fadeInUp" data-wow-delay="0.2s">
                <input type="text" class="form-control" placeholder="Name">
              </div>
              <div class="col-md-12 input-container wow animated fadeInUp" data-wow-delay="0.2s">
                <input type="text" class="form-control" placeholder="Email">
              </div>
              <div class="col-md-12 textarea-container wow animated fadeInUp" data-wow-delay="0.2s">
                <textarea class="form-control" placeholder="Your Message Here"></textarea>
              </div>
              <div class="col-md-12 button-container wow animated fadeInUp" data-wow-delay="0.2s">
                <input type="submit" class="submit-btn def-btn" value="Send Message">
              </div>
            </form>
          </div><!--/.footer-form -->
        </div>
        
      </div>
    </section>