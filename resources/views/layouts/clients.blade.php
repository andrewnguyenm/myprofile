<section class="clients">
      <div class="container">
        <div class="title section-title text-center wow animated fadeIn">
          <div class="title-icon-container">
            <div class="title-icon"><i class="pe-7s-rocket pe-2x pe-va"></i></div>
          </div>
          <h1>Some Clients</h1>
          <h2>Who turst me</h2>
        </div>

        <div class="description">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
        </div>

        <div id="clients" class="clients-logo owl-theme wow animated fadeIn">
          <div class="item">
            <a href="#"><img src="assets/images/client1.png" alt="clients"></a>
          </div>
          <div class="item">
            <a href="#"><img src="assets/images/client2.png" alt="clients"></a>
          </div>
          <div class="item">
            <a href="#"><img src="assets/images/client3.png" alt="clients"></a>
          </div>
          <div class="item">
            <a href="#"><img src="assets/images/client4.png" alt="clients"></a>
          </div>
          <div class="item">
            <a href="#"><img src="assets/images/client5.png" alt="clients"></a>
          </div>
          <div class="item">
            <a href="#"><img src="assets/images/client6.png" alt="clients"></a>
          </div>
          <div class="item">
            <a href="#"><img src="assets/images/client7.png" alt="clients"></a>
          </div>
          <div class="item">
            <a href="#"><img src="assets/images/client8.png" alt="clients"></a>
          </div>
        </div>
      </div><!--/.container -->
    </section><!--/.clients -->