    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Diego - Responsive One Page Template">
    <link rel="icon" href="{{ asset('assets/images/favicon.png') }}">
    <title>MePro</title>

    <!-- Stylesheets -->

    <!-- Bootstrap -->
    <link href="{{ asset('assets/stylesheets/css/bootstrap/bootstrap.css') }}" rel="stylesheet">

    <!-- font-awesome.css -->
    <link href="{{ asset('assets/stylesheets/css/fontawesome/font-awesome.css') }}" rel="stylesheet">

    <!-- pe-icon.css -->
    <link href="{{ asset('assets/stylesheets/css/pe-icon/pe-icon-7-stroke.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/stylesheets/css/pe-icon/helper.css') }}" rel="stylesheet">

    <!-- Animate.css -->
    <link href="{{ asset('assets/stylesheets/css/animate.css') }}" rel="stylesheet">

    <!-- Owl Carousel -->
    <link href="{{ asset('assets/stylesheets/css/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/stylesheets/css/owl-carousel/owl.theme.css') }}" rel="stylesheet">

    <!-- Slick -->
    <link href="{{ asset('assets/stylesheets/css/slick/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/stylesheets/css/slick/slick-theme.css') }}" rel="stylesheet">

    <!-- Main CSS -->
    <link href="{{ asset('assets/stylesheets/css/style.css') }}" rel="stylesheet">
    <!-- <link href="assets/stylesheets/css/color-scheme.css" rel="stylesheet"> -->

    <!-- Font style live -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>

    <!-- Style Switcher -->
    <link href="{{ asset('assets/stylesheets/css/switcher/switcher.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/stylesheets/css/colors/yellow.css') }}" id="switch_style" rel="stylesheet"/>
    @yield('css')