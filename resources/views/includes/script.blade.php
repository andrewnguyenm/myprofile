    <script src="{{ asset('assets/javascripts/jquery.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="{{ asset('assets/javascripts/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/javascripts/typed.min.js') }}"></script>
    <script src="{{ asset('assets/javascripts/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ asset('assets/javascripts/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/javascripts/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/javascripts/jquery.countTo.js') }}"></script>
    <script src="{{ asset('assets/javascripts/wow.min.js') }}"></script>
    <script src="{{ asset('assets/javascripts/slick.js') }}"></script>
    <script src="{{ asset('assets/javascripts/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('assets/javascripts/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/javascripts/custom-map.js') }}"></script>
    <script src="{{ asset('assets/javascripts/custom.js') }}"></script>
 <!--   <script src="{{ asset('assets/javascripts/switcher.js') }}"></script> -->
    <!-- Style Switcher - Demo Only End -->
    @yield('script')
