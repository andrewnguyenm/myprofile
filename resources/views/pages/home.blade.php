@extends('layouts.master')
@section('navbar')
    @include('layouts.navbar')
@stop
@section('content')
    <!-- About Section Begin -->
    @include('layouts.about')
    <!-- About section End -->
    
    <!-- Counter Section Begin-->
    @include('layouts.counter')
    <!-- Counter Section End -->
  
    <!--Portfolio Section Begin -->
    @include('layouts.portfolio')
    <!-- Portfolio Section End -->

    <!-- Testimonial Section Begin-->
    @include('layouts.quote')
    <!-- Testimonial Section End-->

    <!-- Why Choose Me Section Begin  -->
    @include('layouts.whyme')
    <!-- Why Choose Me Section End  -->

    <!-- Education section Begin  -->
    @include('layouts.education')
    <!-- Education section End  -->

    <!-- Employment section Begin  -->
    @include('layouts.employment')
    <!-- Employment section End  -->

    <!-- Pricing Section Start  -->
<!--     @include('layouts.price') -->
    <!-- Pricing Section End  -->

    <!-- Twitter-Feed Section Begin  -->
    @include('layouts.twitter')
    <!-- Twitter-Feed Section End  -->

    <!-- Journal Section Begin  -->
    @include('layouts.journal')
    <!-- Journal Section End  -->

    <!-- Clients Section Begin  -->
    <!-- Clients Section End  -->

    <!-- Map Section Begin  -->
    @include('layouts.map')
    <!-- Map Section End  -->

    <!-- Contact Section Begin  -->
    @include('layouts.contact')
    <!-- Contact Section End  -->
	
@stop