@extends('layouts.master')
@section('navbar')
  @include('layouts.navbar-2')
@stop
@section('content')

<!-- Journal List Section Begin -->
    <section class="journal-list bg-section" id="first-section">
      <div class="title section-title bg-section text-center bottom-shape wow animated fadeIn">
        <div class="title-icon-container">
          <div class="title-icon"><i class="pe-7s-news-paper pe-2x pe-va"></i></div>
        </div>
        <h1>My Blog</h1>
        <h2>Lorem ipsum dolor Sit Amet</h2>
      </div>
      
      <div class="container-medium clearfix">
        <div class="top-search col-md-12 wow animated fadeIn">
          <div class="search-wrap">
            <input type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon1">
            <span class="search-icon" id="basic-addon1"><i class="pe-7s-search"></i></span>
          </div>
        </div>

        <div id="masonry-container">
        @foreach($article as $a)
          <article class="col-md-6 masonry wow animated fadeIn" data-wow-delay="0.2s">
            <div class="journal-content">
              <div class="media">
                <img src="{{ url('assets/images/news1.jpg') }}" alt="news content">
              </div>
              <div class="detail">
                <div class="category-wrap"><div class="category"><i class="pe-7s-photo"></i></div></div>
                <div class="title"><a href="{{ route('blog.detail', $a->id) }}"><h3>{{ $a->title }}</h3></a></div>
                <div class="info">
                  <i class="pe-7s-user"></i><a href="#">{{ $a->author }}</a> <i class="pe-7s-ticket"></i> <a href="#">Freebies</a> <i class="pe-7s-comment"></i> <a href="#">3 Comments</a>
                </div>
                <div class="text">{{ $a->description }}</div>
                <div class="post-date">{{ $a->created_at }}</div>
              </div>
            </div>
          </article>
          @endforeach
        </div><!--/#masonry-container -->
      </div><!--/.container-medium -->

      <!-- Pagination Section Begin -->
      <div class="pagination-wrap wow animated fadeIn">
        <div class="container">
          <ul class="pagination">
            <li>
              <a href="#" aria-label="Previous">
                <span aria-hidden="true">Prev</span>
              </a>
            </li>
            <li><a href="#" class="active">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
              <a href="#" aria-label="Next">
                <span aria-hidden="true">Next</span>
              </a>
            </li>
          </ul><!--/.pagination -->
        </div><!--/.container -->
      </div><!--/.pagination-wrap -->
      <!-- Pagination Section End -->

    </section><!--/.journal-list-->
    <!-- Journal List Section End -->
@stop