@extends('layouts.master')
@section('navbar')
	@include('layouts.navbar-2')
@stop
@section('content')
	@include('layouts.education')
	@include('layouts.employment')
@stop