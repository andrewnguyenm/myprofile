@extends('layouts.master')
@section('navbar')
  @include('layouts.navbar-2')
@stop
@section('content')

<!-- Journal Single Section Begin -->
    <section class="journal-single" id="first-section">
      <div class="title section-title bg-section text-center bottom-shape wow animated fadeIn">
        <div class="title-icon-container">
          <div class="title-icon"><i class="pe-7s-news-paper pe-2x pe-va"></i></div>
        </div>
        <h1>{{ $article->title }}</h1>
        <h2>Lorem ipsum dolor Sit Amet</h2>
      </div>

      <div class="container journal-wrap">
        <!-- Blog Wrap Begin -->
        <section class="journal-detail">
          
          <div class="col-md-8 post-section">

            <div class="post-single post-content">

              <div class="post-inner">
                <div class="post-media">
                  <a href="#"><img src="{{ url('assets/images/news1.jpg') }}" alt="Blog images"></a>
                </div>
                <div class="post-head">
                  <div class="info">
                    <i class="pe-7s-user"></i><a href="#">Administrator</a> <i class="pe-7s-ticket"></i> <a href="#">Freebies</a> <i class="pe-7s-comment"></i> <a href="#">3 Comments</a>
                  </div>
                </div>
                <div class="post-text">
                  {{ $article->content }}
                </div>

                <div class="comment">
                  <div class="title">
                    <h2>Comments</h2>
                  </div>

                  <div class="comment-list indent-one">
                    <div class="comment-container">
                      <div class="avatar"><img src="{{ url('assets/images/person1.jpg') }}" alt="blog avatar"></div>
                      <div class="content">
                        <h4 class="name">
                          Johnny Doe
                          <span><a href="#">Aug 11, 10:05 pm</a></span>
                        </h4><!--/.name-->
                        <div class="text">
                          At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium.
                        </div><!--/.text-->
                        <div class="reply"><a href="#">Reply</a></div>
                      </div><!--/.content-->
                    </div><!--/.comment-container-->
                  </div><!--/.comment-list-->

                  <div class="comment-list indent-two">
                    <div class="comment-container">
                      <div class="avatar"><img src="{{ url('assets/images/person2.jpg') }}" alt="blog avatar"></div>
                      <div class="content">
                        <h4 class="name">
                          Tiffany Morey
                          <span><a href="#">Aug 11, 10:05 pm</a></span>
                        </h4><!--/.name-->
                        <div class="text">
                          At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium.
                        </div><!--/.text-->
                        <div class="reply"><a href="#">Reply</a></div>
                      </div><!--/.content-->
                    </div><!--/.comment-container-->
                  </div><!--/.comment-list-->

                  <div class="comment-list indent-one">
                    <div class="comment-container">
                      <div class="avatar"><img src="{{ url('assets/images/person3.jpg') }}" alt="blog avatar"></div>
                      <div class="content">
                        <h4 class="name">
                          Colin Stewart
                          <span><a href="#">Aug 11, 10:05 pm</a></span>
                        </h4><!--/.name-->
                        <div class="text">
                          At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium.
                        </div><!--/.text-->
                        <div class="reply"><a href="#">Reply</a></div>
                      </div><!--/.content-->
                    </div><!--/.comment-container-->
                  </div><!--/.comment-list-->

                  <div class="comment-list indent-one">
                    <div class="comment-container">
                      <div class="avatar"><img src="{{ url('assets/images/person3.jpg') }}" alt="blog avatar"></div>
                      <div class="content">
                        <h4 class="name">
                          Tessa William
                          <span><a href="#">Aug 11, 10:05 pm</a></span>
                        </h4><!--/.name-->
                        <div class="text">
                          At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium.
                        </div><!--/.text-->
                        <div class="reply"><a href="#">Reply</a></div>
                      </div><!--/.content-->
                    </div><!--/.comment-container-->
                  </div><!--/.comment-list-->

                </div><!--/.comment-->

                <div class="respond">
                  <div class="title">
                    <h2>Leave a reply</h2>
                  </div>
                  <form action="#">
                    <div class="row">
                      <div class="col-md-4">
                        <label for="author">Name<span class="required">*</span></label>
                        <input id="author" class="form-control" name="author" type="text" required>
                      </div>
                      <div class="col-md-4">
                        <label for="email">Email<span class="required">*</span></label>
                        <input id="email" class="form-control" name="author" type="text" required>
                      </div>
                      <div class="col-md-4">
                        <label for="url">Website<span class="required">*</span></label>
                        <input id="url" class="form-control" name="url" type="text" required>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label for="comment">Add Your Comment</label>
                        <textarea id="comment" class="form-control" name="comment" required></textarea>
                        <input name="submit" type="submit" id="submit" class="def-btn" value="Submit Comment">
                      </div>
                    </div>
                  </form>
                </div><!--/.respond-->
              </div><!-- /.post-inner -->
            </div><!-- /.post-content -->

          </div><!-- /.post-section -->
        </section><!-- /.blog-detail -->

        <section class="journal-sidebar col-md-4">

          <div class="search-post">
            <div class="title">
              <h3><span class="shape"><i class="pe-7s-search"></i></span> Search</h3>
            </div>
            <div class="content">
              <div class="search-wrap">
                <input type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon1">
                <span class="search-icon" id="basic-addon1"><i class="pe-7s-search"></i></span>
              </div>
            </div><!--/.content-->
          </div><!--/.search-post-->

          <div class="post-categories">
            <div class="title">
              <h3><span class="shape"><i class="pe-7s-ribbon"></i></span> Categories</h3>
            </div>
            <div class="content">
              <ul>
                <li>
                  <a href="#">
                    <i class="icon-caret-right"></i> News And Update <span>(13)</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon-caret-right"></i> Interviews <span>(7)</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon-caret-right"></i> Graphic And Web Design <span>(20)</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon-caret-right"></i> Wordpress Hacks <span>(34)</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon-caret-right"></i> Freebies <span>(17)</span>
                  </a>
                </li>
              </ul>
            </div><!--/.content-->
          </div><!--/.post-categories-->

          <div class="recent-post">
            <div class="title">
              <h3><span class="shape"><i class="pe-7s-clock"></i></span> Recent Post</h3>
            </div>
            <div class="content">
              <ul>
                <li>
                  <a href="#">Download My Design Work</a>
                  <span class="post-date">February 28, 2015</span>
                </li>
                <li>
                  <a href="#">Mobile App Development begin</a>
                  <span class="post-date">January 28, 2015</span>
                </li>
                <li>
                  <a href="#">Just Photography Test</a>
                  <span class="post-date">January 30, 2015</span>
                </li>
                <li>
                  <a href="#">My New Videography Project</a>
                  <span class="post-date">March 30, 2015</span>
                </li>
              </ul>
            </div><!--/.content-->
          </div><!--/.recent-post-->

          <div class="post-archives">
            <div class="title">
              <h3><span class="shape"><i class="pe-7s-folder"></i></span> Archives</h3>
            </div>
            <div class="content">
              <ul>
                <li>
                  <a href="#"><i class="icon-caret-right"></i> January <span>(13)</span></a>
                </li>
                <li>
                  <a href="#"><i class="icon-caret-right"></i> February <span>(7)</span></a>
                </li>
                <li>
                  <a href="#"><i class="icon-caret-right"></i> March <span>(20)</span>
                  </a>
                </li>
                <li>
                  <a href="#"><i class="icon-caret-right"></i> April <span>(34)</span></a>
                </li>
              </ul>
            </div><!--/.content-->
          </div><!--/.post-archives-->

          <div class="post-tags">
            <div class="title">
              <h3><span class="shape"><i class="pe-7s-ticket"></i></span>Popular Tags</h3>
            </div>
            <div class="content">
              <a href="#"><span class="tag-label">Freebies</span></a>
              <a href="#"><span class="tag-label">Developer</span></a>
              <a href="#"><span class="tag-label">Design</span></a>
              <a href="#"><span class="tag-label">Tutorial</span></a>
              <a href="#"><span class="tag-label">Inside Us</span></a>
            </div><!--/.content-->
          </div><!--/.post-tags-->

        </section>
      </div>

    </section><!--/.journal-single-->
    <!-- Journal Single Section End -->
@stop
