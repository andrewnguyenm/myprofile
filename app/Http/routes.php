<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// routes pages 
Route::get('/', [
	'as' => 'pages.home',
	'uses' => 'PagesController@index'
	]);

Route::get('/about-me',[
	'as' => 'pages.aboutme',
	'uses' => 'PagesController@aboutme'
	]);

Route::get('/contact',[
	'as' => 'pages.contact',
	'uses' => 'PagesController@contact'
	]);
// routes project 
Route::get('/my-project',[
	'as' => 'project.list',
	'uses' => 'ProjectsController@index'
]);

Route::get('/my-project/{id}',[
	'as' => 'project.detail',
	'uses' => 'ProjectsController@show'
	]);
// routes blog 
Route::get('/blog',[
	'as' => 'blog.list',
	'uses' => 'PostsController@index'
	]);

Route::get('/blog/{id}',[
	'as' => 'blog.detail',
	'uses' => 'PostsController@show'
	]);
// routes edu-exp
Route::get('/experience',[
	'as' => 'exps.list',
	'uses' => 'ExpsController@index'
	]);

