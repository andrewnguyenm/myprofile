<?php

use Illuminate\Database\Seeder;
use  App\Article;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        for($i = 0; $i < 10; ++$i) {
            Article::create([
                'title' => $faker->sentence,
                'images' => $faker->imageUrl($width = 640, $height = 480),
                'description' => $faker->sentence,
                'content' => implode('', $faker->sentences(20)),
                'author' => $faker->word,
                'categories' => $faker->randomDigitNotNull,
                'status' => $faker->randomDigitNotNull

            ]);
        }
    }
}
