<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        for($i = 0; $i < 10; ++$i) {
            Category::create([
                'title' => $faker->sentence,
                'description' => $faker->sentence,
                'status' => $faker->randomDigitNotNull
            ]);
    }
}
